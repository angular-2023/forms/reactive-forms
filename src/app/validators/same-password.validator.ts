import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function samePasswordValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const isSame = control.value.password === control.value.confirmPassword;

    console.log(isSame);

    return isSame ? null : { samePassword: { value: control.value } };
  };
}
